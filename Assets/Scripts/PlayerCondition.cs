using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;


public class PlayerCondition : MonoBehaviour
{
    public int healthPoints;
    public int maxHp;
    //public TextMeshPro healthBar;
    //public Slider healthSlider;
    //public AudioSource hitSound;
    //public AudioSource deathSound;
    EnemyBehaviour enemyBehaviour;




    // Start is called before the first frame update
    void Start()
    {
        CheckHealth();
    }

    // Update is called once per frame
    void Update()
    {
        if (healthPoints <= 0)
        {
            print ("je suis mort");
        }
    }
    void CheckHealth()
    {
        if (healthPoints >= maxHp)
        {
            healthPoints = maxHp;
        }
    }

    public void TakeDamage(int receiveDamage)
    {
        healthPoints = healthPoints - receiveDamage;

    }


}
