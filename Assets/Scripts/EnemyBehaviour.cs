using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;


public class EnemyBehaviour : MonoBehaviour
{
    public int healthPoints;
    public int maxHp;
    public int strikeDamage = 10;
    //public TextMeshPro healthBar;
    //public Slider healthSlider;
    //public AudioSource hitSound;
    //public AudioSource deathSound;
    WeaponInventory weaponInventory;


    public float attackDistance = 3f;
    public float movementSpeed = 4f;
    public float attackRate = 0.5f;




    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        //healthSlider.value = (float)healthPoints / (float)maxHp;
        if (healthPoints <= 0)
        {
            //deathSound.Play();
            Destroy(gameObject);

        }

        CheckHealth();
    }

    void CheckHealth()
    {
        if (healthPoints >= maxHp)
        {
            healthPoints = maxHp;
        }
    }

    void DamageEnemy()
    {
        int getWeaponDamage = weaponInventory.selectedItem.weaponDamage;

        healthPoints -= getWeaponDamage;
    }


    void OnTriggerEnter(Collider col)
    {
        if (col.tag == "Player")
        {
           col.GetComponent <PlayerCondition>().TakeDamage(strikeDamage);

        }
    }


}

