using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class WeaponInventory : MonoBehaviour
{
    public int currentItem = 0;
    [SerializeField] int previousItem = 1;


    [SerializeField] Image weaponIconUI;
    public InventoryItem selectedItem;

    public List<InventoryItem> items;
    public TextMeshProUGUI txtammoUI;
    public TextMeshProUGUI txtTotalAmmoUI;


    // Start is called before the first frame update
    void Start()
    {
        //Initialize the inventory en select the first item.
        foreach (InventoryItem item in items)
        {
            item.weaponPrefab.SetActive(false);

        }

        //   RefreshInventory (currentItem);
        selectedItem = items[currentItem];
        RefreshInventory(currentItem);

        previousItem = currentItem;

    }

    // Update is called once per frame
    void Update()
    {
        SwitchItem();
    }

    void SwitchItem()
    {
        if (Input.GetAxis("Mouse ScrollWheel") != 0)
        {
            float mouseScroll = Input.GetAxis("Mouse ScrollWheel") * 10;
            currentItem += (int)mouseScroll;
            //        print (mouseScroll);
            currentItem = currentItem < 0 ? items.Count - 1 : currentItem % items.Count;
            if (currentItem != previousItem)
            {
                RefreshInventory(currentItem);
            }
        }

    }

    public void RefreshInventory(int curItem)
    {
        print(curItem);
        print(selectedItem.name);

        //When the currentItem value does not equal previousItem, execute the swap.


        print($"{curItem},{previousItem}");
        //Set item to current index 
        selectedItem = items[curItem];

        //set the selected item active.
        selectedItem.weaponPrefab.SetActive(selectedItem.selectable);
       /* weaponIconUI.sprite = selectedItem.icon;
        //        print(selectedItem.weaponPrefab.activeSelf);
        UpdateAmmoUI(selectedItem.currentAmmo, selectedItem.maxAmmo);
        UpdateTotalAmmoUI(selectedItem.totalAmmoAmount);*/
        print(selectedItem.name);
        //disable previous selected item.
        if (items.Count >= 2)
        {
            items[previousItem].weaponPrefab.SetActive(false);
        }

        previousItem = curItem;


    }

    public void UpdateAmmoUI(int currentAmmo, int maxAmmo)
    {

        txtammoUI.SetText(currentAmmo.ToString() + "/" + maxAmmo.ToString());

    }
    public void UpdateTotalAmmoUI(int totalAmmoAmount)
    {

        txtTotalAmmoUI.SetText("Ammo: " + totalAmmoAmount.ToString());

    }
}

[Serializable]
public class InventoryItem
{

    public GameObject weaponPrefab;

    public bool selectable = false;

    public string name;
    public Sprite icon;

    public int weaponDamage = 10;
    public int totalAmmoAmount = 100;
    public int ammoUsedPerShot = 1;
    public int currentAmmo = 10;
    public int maxAmmo = 10;
    public float fireRate = 1f;

    public float maxDist = 100f;
    public float bloom = 10f;
    public LayerMask hitMask;
    public int burst;
    public Transform rayOrigin;

}