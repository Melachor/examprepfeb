using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Animations;

public class PlayerAnimationController : MonoBehaviour
{
    [SerializeField] Animator anim;

    [SerializeField] float currentSpeed = 0f;
    [SerializeField] CharacterController playerobj;
    [SerializeField] AudioSource weaponAudio;
    WeaponInventory weaponInventory;

    Vector3 oldPosition;

    void Start()
    {
        if (anim == null)
        {
            anim = GetComponent<Animator>();
        }
        weaponInventory = playerobj.GetComponent<WeaponInventory>();
    }
    void Update()
    {
        if (playerobj != null)
        {

            currentSpeed = new Vector3(playerobj.velocity.x, 0f, playerobj.velocity.z).magnitude;
            print($"dir:{currentSpeed}");

        }

        anim.SetFloat("_Speed", currentSpeed);
        fireWeapon();
        ReloadWeapon();

    }


    void fireWeapon()
    {
        if (Input.GetButtonDown("Fire1"))
        {
            int currentAmmo = weaponInventory.selectedItem.currentAmmo;

            if (currentAmmo > 0)
            {
                anim.SetTrigger("_FireGun");
            }
        }
    }
    void ReloadWeapon()
    {
        if (Input.GetButtonDown("Reload"))

        {
            int totalAmmoLeft = weaponInventory.selectedItem.totalAmmoAmount;

            if (totalAmmoLeft > 0)
            {
                anim.SetTrigger("_Reload");
            }



        }
    }
    void UpdateAmmo()
    {

        InventoryItem selectedItem = weaponInventory.selectedItem;
        int currentAmmo = selectedItem.currentAmmo;
        int maxAmmo = selectedItem.maxAmmo;
        int ammoUsedPerShot = selectedItem.ammoUsedPerShot;

        while (ammoUsedPerShot > 0 && currentAmmo > 0)
        {
            currentAmmo--;
            ammoUsedPerShot--;
            selectedItem.currentAmmo = currentAmmo;
            weaponInventory.UpdateAmmoUI(currentAmmo, maxAmmo);
        }
    }
    void RefillAmmo()
    {

        InventoryItem selectedItem = weaponInventory.selectedItem;
        int currentAmmo = selectedItem.currentAmmo;
        int maxAmmo = selectedItem.maxAmmo;
        int totalAmmoAmount = selectedItem.totalAmmoAmount;

        int ammoNeeded = maxAmmo - currentAmmo;

        if (totalAmmoAmount >= ammoNeeded)
        {
            currentAmmo = maxAmmo;
            totalAmmoAmount -= ammoNeeded;
        }
        else
        {
            currentAmmo += totalAmmoAmount;
            totalAmmoAmount = 0;
        }

        selectedItem.currentAmmo = currentAmmo;
        selectedItem.totalAmmoAmount = totalAmmoAmount;

        weaponInventory.UpdateAmmoUI(currentAmmo, maxAmmo);
        weaponInventory.UpdateTotalAmmoUI(totalAmmoAmount);
    }
    void PlaySound(AudioClip soundClip)
    {
        weaponAudio.PlayOneShot(soundClip);
    }


    void ShootBullet()
    {

        float getBloom = weaponInventory.selectedItem.bloom;
        float getMaxDist = weaponInventory.selectedItem.maxDist;
        LayerMask getHitMask = weaponInventory.selectedItem.hitMask;
        int getBurst = weaponInventory.selectedItem.burst;
        Transform getRayOrigin = weaponInventory.selectedItem.rayOrigin;


        for (int i = 0; i < getBurst; i++)
        {
            float x = 0f, y = 0f;

            x = Random.Range(-getBloom, getBloom);
            y = Random.Range(-getBloom, getBloom);

            Quaternion rayOffset = Quaternion.Euler(new Vector3(x, y, 0));
            Vector3 newDirection = rayOffset * getRayOrigin.forward;

            Ray ray = new Ray(getRayOrigin.position, newDirection);

            RaycastHit hit = new RaycastHit();

            if (Physics.Raycast(ray, out hit, getMaxDist, getHitMask))
            {
                if (hit.collider.GetComponent<EnemyBehaviour>() != null)

                {
                    EnemyBehaviour getEnemy = hit.collider.GetComponent<EnemyBehaviour>();

                    getEnemy.healthPoints -= weaponInventory.selectedItem.weaponDamage;
                    print(getEnemy.healthPoints);

                }
                Color color = Color.red;
                Debug.DrawLine(getRayOrigin.position, hit.point, Color.red, 20.0f);
                print($"HitObject: {hit.transform.name}, HitPos: {hit.point}, HitNrml: {hit.normal}");
            }
            else
            {
                Debug.DrawRay(getRayOrigin.position, newDirection * getMaxDist, Color.blue, 20.0f);
            }
        }
    }
}

